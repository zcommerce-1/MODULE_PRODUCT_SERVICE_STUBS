import com.google.googlejavaformat.java.JavaFormatterOptions
import com.google.googlejavaformat.java.Formatter

buildscript {
	dependencies {
		classpath 'com.google.googlejavaformat:google-java-format:1.17.0'
	}
}

plugins {
	id 'java'
	id 'org.springframework.boot' version '3.1.0'
	id 'io.spring.dependency-management' version '1.1.0'
	id "com.diffplug.spotless" version "6.20.0"
}

group = 'com.crio.zcommerce.backend'
version = '0.0.1-SNAPSHOT'
sourceCompatibility = '17'

configurations {
	compileOnly {
		extendsFrom annotationProcessor
	}
}

repositories {
	mavenCentral()
}

dependencies {
	implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
	implementation 'org.springframework.boot:spring-boot-starter-data-redis'
//	implementation 'org.liquibase:liquibase-core'
	implementation 'org.springframework.kafka:spring-kafka'
	implementation 'org.projectlombok:lombok:1.18.22'
	compileOnly 'org.projectlombok:lombok'
	annotationProcessor 'org.projectlombok:lombok'
	testImplementation 'org.springframework.boot:spring-boot-starter-test'
	testImplementation 'org.springframework.kafka:spring-kafka-test'
	implementation 'org.springframework.boot:spring-boot-starter-validation'
	implementation 'org.modelmapper:modelmapper:3.1.1'
	implementation 'org.springframework.boot:spring-boot-starter-web'
	implementation 'org.springframework.metrics:spring-metrics:0.5.1.RELEASE'
	implementation group: 'com.squareup.okhttp3', name: 'okhttp', version: '4.11.0'
	testImplementation 'org.springframework.security:spring-security-test'
	implementation group: 'mysql', name: 'mysql-connector-java', version:'8.0.28'
	testImplementation 'com.h2database:h2:2.1.210'
	implementation 'org.springdoc:springdoc-openapi-starter-webmvc-ui:2.0.0'
	implementation 'org.springframework.boot:spring-boot-starter-actuator'
	implementation('io.opentelemetry.instrumentation:opentelemetry-instrumentation-annotations:1.32.0')
	implementation group: 'net.logstash.logback', name: 'logstash-logback-encoder', version: '7.3'
	implementation 'org.mapstruct:mapstruct:1.4.2.Final'
	annotationProcessor 'org.mapstruct:mapstruct-processor:1.4.2.Final'
	implementation 'org.springframework.boot:spring-boot-starter-security'
	implementation 'org.springframework.boot:spring-boot-starter-oauth2-resource-server'
	implementation 'org.springframework.security:spring-security-oauth2-jose:6.1.2'
	implementation 'com.auth0:jwks-rsa:0.14.0'
	implementation 'io.jsonwebtoken:jjwt-api:0.11.2'
	implementation 'io.jsonwebtoken:jjwt-impl:0.11.2'
	implementation 'io.jsonwebtoken:jjwt-jackson:0.11.2'

}

tasks.named('test') {
	useJUnitPlatform()
}

spotless {
	format 'misc', {
		// define the files to apply `misc` to
		target '*.gradle', '*.md', '.gitignore'

		// define the steps to apply to those files
		trimTrailingWhitespace()
		indentWithTabs() // or spaces. Takes an integer argument if you don't like 4
		endWithNewline()
	}

	java {
		// Use the default importOrder configuration
		importOrder()

		removeUnusedImports()

		// Cleanthat will refactor your code, but it may break your style: apply it before your formatter
		cleanthat()

		// Apply formatter
		custom 'google-java-format', { String source ->
			JavaFormatterOptions options = JavaFormatterOptions.builder()
					.style(JavaFormatterOptions.Style.GOOGLE)
					.formatJavadoc(false)
					.build()
			Formatter formatter = new Formatter(options)
			return formatter.formatSource(source)
		}

		formatAnnotations()  // fixes formatting of type annotations
	}
}
