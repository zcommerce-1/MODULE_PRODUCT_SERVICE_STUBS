package com.crio.zcommerce.backend;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class ProductApplication {

  @GetMapping("/")
  public String home() {
    return "Welcome to ZCommerce - Product";
  }

  public static void main(String[] args) {
    SpringApplication.run(ProductApplication.class, args);
  }

  @Configuration
  public class ModelMapperConfig {
    @Bean
    public ModelMapper modelMapper() {
      return new ModelMapper();
    }
  }
}
