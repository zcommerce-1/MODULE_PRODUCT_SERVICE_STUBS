package com.crio.zcommerce.backend.product.model.enums;

public enum SortBy {
  DATE_ADDED,

  AVAILABILITY,

  DISCOUNT,

  BEST_SELLER,

  PRICE_HIGH_TO_LOW,

  PRICE_LOW_TO_HIGH
}
