package com.crio.zcommerce.backend.product.service.implementation;

import com.crio.zcommerce.backend.product.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.product.dao.ItemDao;
import com.crio.zcommerce.backend.product.model.Category;
import com.crio.zcommerce.backend.product.model.Inventory;
import com.crio.zcommerce.backend.product.model.Item;
import com.crio.zcommerce.backend.product.model.Review;
import com.crio.zcommerce.backend.product.model.enums.ReviewIncludeType;
import com.crio.zcommerce.backend.product.model.enums.SortBy;
import com.crio.zcommerce.backend.product.service.CategoryService;
import com.crio.zcommerce.backend.product.service.ItemService;
import com.crio.zcommerce.backend.product.service.ReviewService;
import java.util.HashSet;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@Log4j2
@RequiredArgsConstructor
public class ItemServiceImpl implements ItemService {

  private final ItemDao itemDao;

  private final ReviewService reviewService;

  private final CategoryService categoryService;

  @Override
  public Item getItemDetails(Long itemId, ReviewIncludeType reviewIncludeType)
      throws EntityDoesNotExistException {
    String logPrefix = "event: getItemDetails, message: ";
    log.info(logPrefix + "fetching item details for itemId: {}", itemId);
    Optional<Item> itemOptional = itemDao.findByItemId(itemId);
    if (itemOptional.isPresent()) {
      Item item = itemOptional.get();
      if (reviewIncludeType.equals(ReviewIncludeType.INCLUDE)) {
        Page<Review> review = reviewService.getReviewsOfItem(itemId, PageRequest.of(0, 10));
        item.setReviewsList(new HashSet<>());
        item.getReviewsList().addAll(review.toList());
      }
      log.info(logPrefix + "item details is fetched for itemId : {}", itemId);
      return item;
    } else {
      log.info(logPrefix + "item details not found for the itemId : {}", itemId);
      throw new EntityDoesNotExistException(
          String.format("%s Item details not found for the itemId : %s", logPrefix, itemId));
    }
  }

  @Override
  public Item addItem(Item item) throws EntityDoesNotExistException {
    String logPrefix = "event: addItem, message: ";
    log.info(logPrefix + "adding Item: {}", item);
    // check if the category exists
    Category category = categoryService.getCategoryDetails(item.getCategory().getId());
    item.setCategory(category);

    Item addedItem = itemDao.addItem(item);
    log.info(
        logPrefix + "new item got added, id: {} , code: {}",
        addedItem.getId(),
        addedItem.getCode());

    //    // todo check this - krishna
    //    // create inventory entry
    //    item.getInventory().setItem(addedItem);
    //    Inventory addedInventory = itemDao.addInventory(item.getInventory());
    //    addedItem.setInventory(addedInventory);

    return addedItem;
  }

  @Override
  public Item updateItem(Long itemId, Item item) throws EntityDoesNotExistException {
    String logPrefix = "event: updateCart, message: ";
    log.info(logPrefix + "updating Item with itemId: {}", itemId);
    Item updatedItem = itemDao.updateItem(itemId, item);
    log.info(
        logPrefix + "item has been updated, id: {} , item: {}", updatedItem.getId(), updatedItem);
    return updatedItem;
  }

  @Override
  public void deleteItem(Long itemId) {
    String logPrefix = "event: updateCart, message: ";
    log.info(logPrefix + "item is being deleted, id: {}", itemId);
    itemDao.deleteItem(itemId);
  }

  @Override
  public Page<Item> searchForItem(String searchKeyword, SortBy sortBy, Pageable pageable) {
    return itemDao.searchForItem(searchKeyword, sortBy, pageable);
  }

  @Override
  public Inventory getInventoryDetails(Long itemId) throws EntityDoesNotExistException {
    String logPrefix = "event: getInventoryDetails, message: ";
    log.info(logPrefix + "getting inventory details for itemId: {}", itemId);
    Optional<Inventory> inventoryOptional = itemDao.findInventoryByItemId(itemId);
    if (inventoryOptional.isPresent()) {
      return inventoryOptional.get();
    } else {
      log.info(logPrefix + "inventory details not found for the itemId : {}", itemId);
      throw new EntityDoesNotExistException(
          String.format("%s inventory details not found for the itemId : %s", logPrefix, itemId));
    }
  }

  @Override
  public void updateInventory(Long itemId, Inventory inventory) {
    itemDao.updateInventory(itemId, inventory);
  }
}
