package com.crio.zcommerce.backend.product.model.enums;

public enum ReviewIncludeType {
  INCLUDE,

  NOT_INCLUDE,
}
