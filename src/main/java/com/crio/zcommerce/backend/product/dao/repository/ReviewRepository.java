package com.crio.zcommerce.backend.product.dao.repository;

import com.crio.zcommerce.backend.product.dao.entity.ReviewEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReviewRepository extends JpaRepository<ReviewEntity, Long> {

  // todo check if item or itemId should be passed
  Page<ReviewEntity> findByItemId(Long itemId, Pageable pageable);
}
