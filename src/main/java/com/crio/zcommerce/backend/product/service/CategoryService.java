package com.crio.zcommerce.backend.product.service;

import com.crio.zcommerce.backend.product.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.product.model.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CategoryService {

  /**
   * Get all category
   *
   * @return list of category in paginated mode
   */
  Page<Category> getAllCategory(Pageable pageable);

  /**
   * Get category details for the given categoryId
   *
   * @param categoryId of type Long
   * @return category details if present
   * @throws EntityDoesNotExistException when category details is not found for given category id
   */
  Category getCategoryDetails(Long categoryId) throws EntityDoesNotExistException;

  /**
   * Add or create category
   *
   * @param category of type Category
   * @return category details of the actual category created
   */
  Category addCategory(Category category);

  /**
   * Update category
   *
   * @param categoryId of type long
   * @param category of type Category to update
   * @return updated item details
   * @throws EntityDoesNotExistException when category details is not found against given categoryId
   */
  Category updateCategory(Long categoryId, Category category) throws EntityDoesNotExistException;

  /**
   * Delete category details
   *
   * @param categoryId of type Long
   */
  void deleteCategory(Long categoryId);
}
