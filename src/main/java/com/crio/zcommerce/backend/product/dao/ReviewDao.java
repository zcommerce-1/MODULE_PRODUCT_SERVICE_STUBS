package com.crio.zcommerce.backend.product.dao;

import com.crio.zcommerce.backend.product.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.product.model.Review;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ReviewDao {
  Optional<Review> findByReviewId(Long reviewId);

  Page<Review> findAllByItemId(Long itemId, Pageable pageable);

  Review addReview(Review review);

  Review updateReview(Long reviewId, Review review) throws EntityDoesNotExistException;

  void deleteByReviewId(Long reviewId);
}
