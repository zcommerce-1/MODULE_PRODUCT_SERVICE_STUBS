package com.crio.zcommerce.backend.product.dao.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import java.util.Objects;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(
    name = "category",
    uniqueConstraints = {
      @UniqueConstraint(columnNames = "code"),
      @UniqueConstraint(columnNames = "name"),
    },
    indexes = @Index(name = "IDX_name", columnList = "name"))
public class CategoryEntity extends BaseEntity {

  @NotNull private String name;

  @NotNull private String code;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
      return false;
    }
    CategoryEntity that = (CategoryEntity) o;
    return getId() != null && Objects.equals(getId(), that.getId());
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}
