package com.crio.zcommerce.backend.product.common.exceptions;

import java.io.IOException;

public class NetworkException extends IOException {

  private static final long serialVersionUID = 1L;

  public NetworkException(String message) {
    super(message);
  }
}
