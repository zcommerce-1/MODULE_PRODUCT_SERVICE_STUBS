package com.crio.zcommerce.backend.product.config.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

public class CognitoTokenFilter extends OncePerRequestFilter {


  private final CognitoConfig config;

  public CognitoTokenFilter(CognitoConfig config) {
    this.config = config;
  }

  private RequestMatcher permitAllRoutesMatcher() {
    List<RequestMatcher> permitAllMatchers =
        Arrays.asList(
            new AntPathRequestMatcher("/swagger-ui/**"),
            new AntPathRequestMatcher("/v3/api-docs/**"),
            new AntPathRequestMatcher("/public/**"),
            new AntPathRequestMatcher("/api/v1/item/**", "GET"),
            new AntPathRequestMatcher("/api/v1/review", "GET"),
            new AntPathRequestMatcher("/api/v1/review/{id}", "GET"),
            new AntPathRequestMatcher("/api/v1/category/**, ", "GET"),
            new AntPathRequestMatcher("G"));
    return new OrRequestMatcher(permitAllMatchers);
  }

  private boolean isPermitAllEndpoint(HttpServletRequest request) {
    return permitAllRoutesMatcher().matches(request);
  }

  @Override
  protected void doFilterInternal(
      @NotNull HttpServletRequest request,
      @NotNull HttpServletResponse response,
      @NotNull FilterChain filterChain)
      throws ServletException, IOException {

    // If it's a permit-all endpoint, proceed without checking token
    if (isPermitAllEndpoint(request)) {
      filterChain.doFilter(request, response);
      return;
    }

    // Get token
    String token = request.getHeader("Authorization");
    if (token != null && token.startsWith("Bearer ")) {
      token = token.substring(7);
    }


    // If Cognito Token
    Jws<Claims> claims = SecurityUtil.validateToken(token, config.getIssuerUri());

    // Assign granted Authorities (cognito:group as ROLES)
    List<GrantedAuthority> grantedAuthorities =
        Optional.ofNullable((List<String>) claims.getBody().get("cognito:groups"))
            .orElse(Collections.emptyList())
            .stream()
            .map(group -> new SimpleGrantedAuthority("ROLE_" + group))
            .collect(Collectors.toList());

    // Set Authentication in SecurityContextHolder, username as Principal.
    SecurityContextHolder.getContext()
        .setAuthentication(
            new UsernamePasswordAuthenticationToken(
                claims.getBody().get("username"), null, grantedAuthorities));

    filterChain.doFilter(request, response);
  }
}
