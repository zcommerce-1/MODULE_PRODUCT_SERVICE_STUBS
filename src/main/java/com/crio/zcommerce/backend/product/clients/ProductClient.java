package com.crio.zcommerce.backend.product.clients;

import com.crio.zcommerce.backend.product.common.exceptions.ApiException;
import com.crio.zcommerce.backend.product.common.exceptions.NetworkException;
import com.crio.zcommerce.backend.product.config.AppConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import okhttp3.Headers;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Log4j2
public class ProductClient {

  private static final String PRODUCT_API = "api/v1/product";

  private Headers headers;

  private final AppConfig appConfig;

  private final NetworkClient networkClient;

  //  @PostConstruct
  //  void init() {
  //    // todo add valid header fields to making intra service calls
  //    // headers = new Headers.Builder().set("Api-username",
  // appConfig.getAdminUserName()).build();
  //  }

  public Object getProductDetails(Long id) throws NetworkException, ApiException {
    // todo code to call the api and return the product details
    return new Object();
  }
}
