package com.crio.zcommerce.backend.product.config.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.oauth2.server.resource.web.authentication.BearerTokenAuthenticationFilter;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig {

  @Bean
  public SecurityFilterChain filterChain(HttpSecurity http, CognitoConfig cognitoConfig)
      throws Exception {
    // todo: Enable Csrf
    http.csrf(AbstractHttpConfigurer::disable)
        .authorizeHttpRequests(
            auth ->
                auth.requestMatchers("/swagger-ui/**", "/v3/api-docs/**")
                    .permitAll()
                    .requestMatchers(HttpMethod.GET, "/api/v1/item/**")
                    .permitAll()
                    .requestMatchers(HttpMethod.GET, "/api/v1/review")
                    .permitAll()
                    .requestMatchers(HttpMethod.GET, "/api/v1/review/{id}")
                    .permitAll()
                    .requestMatchers(HttpMethod.GET, "/api/v1/category/**")
                    .permitAll()
                    .anyRequest()
                    .authenticated() // All other URLs require authentication
            )
        .addFilterBefore(
            new CognitoTokenFilter(cognitoConfig), BearerTokenAuthenticationFilter.class);
    return http.build();
  }
}
