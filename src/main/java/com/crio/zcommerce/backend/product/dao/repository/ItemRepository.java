package com.crio.zcommerce.backend.product.dao.repository;

import com.crio.zcommerce.backend.product.dao.entity.ItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ItemRepository
    extends JpaRepository<ItemEntity, Long>, JpaSpecificationExecutor<ItemEntity> {}
