package com.crio.zcommerce.backend.product.model;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class Item extends BaseModel {

  @NotNull private String code;

  @NotNull private String name;

  @NotNull private String title;

  private List<String> imageUrls;

  private Double price;

  @Min(value = 0, message = "Must be between 0 to 100")
  @Max(value = 100, message = "Must be between 0 to 100")
  @NotNull @Builder.Default
  private Float discountPercentage = 0f;

  @NotNull private String shortDescription;

  private String longDescription;

  private Inventory inventory;

  private Set<Review> reviewsList;

  @Builder.Default private Float averageRating = 0f;

  @Builder.Default private Integer numberOfReviews = 0;

  @NotNull private Category category;
}
