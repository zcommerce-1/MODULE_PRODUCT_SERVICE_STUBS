package com.crio.zcommerce.backend.product.config;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

  @ExceptionHandler(DataIntegrityViolationException.class)
  public ResponseEntity<String> handleDataIntegrityViolationException(
      DataIntegrityViolationException ex) {
    // You can log the exception details here
    return ResponseEntity.status(HttpStatus.CONFLICT) // or HttpStatus.BAD_REQUEST
        .body("Data integrity violation: " + ex.getMessage());
  }

  // other exception handlers...
}
