package com.crio.zcommerce.backend.product.dao;

import com.crio.zcommerce.backend.product.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.product.model.Inventory;
import com.crio.zcommerce.backend.product.model.Item;
import com.crio.zcommerce.backend.product.model.enums.SortBy;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ItemDao {
  Optional<Item> findByItemId(Long itemId);

  Item addItem(Item item);

  Item updateItem(Long itemId, Item item) throws EntityDoesNotExistException;

  void deleteItem(Long itemId);

  Page<Item> searchForItem(String searchKeyword, SortBy sortBy, Pageable pageable);

  Inventory addInventory(Inventory inventory);

  Optional<Inventory> findInventoryByItemId(Long itemId);

  void updateInventory(Long itemId, Inventory inventory);
}
