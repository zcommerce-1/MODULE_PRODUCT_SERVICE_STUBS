package com.crio.zcommerce.backend.product.controller;

import com.crio.zcommerce.backend.product.common.api.ApiResponse;
import com.crio.zcommerce.backend.product.common.api.ApiResponseStatus;
import com.crio.zcommerce.backend.product.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.product.model.Category;
import com.crio.zcommerce.backend.product.service.CategoryService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.metrics.annotation.Timed;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Timed
@RestController
@RequiredArgsConstructor
@RequestMapping(CategoryController.REVIEW_API_PREFIX)
public class CategoryController {

  public static final String REVIEW_API_PREFIX = "api/v1/category";
  private static final String ID_ENDPOINT = "/{id}";

  private static final String ALL_ENDPOINT = "/all";
  private final CategoryService categoryService;

  /**
   * URL: /api/v1/category/all
   * Description: Get all categories
   * Method: GET
   *
   * @return list of category details
   */
  @GetMapping(ALL_ENDPOINT)
  @Timed
  public ResponseEntity<ApiResponse<Page<Category>>> getAllCategory(
      @RequestParam(defaultValue = "0") Integer page,
      @RequestParam(defaultValue = "20") Integer size) {
    ApiResponse<Page<Category>> response = new ApiResponse<>();
    Page<Category> pagedCategory = categoryService.getAllCategory(PageRequest.of(page, size));
    response.setData(pagedCategory);
    response.setStatus(ApiResponseStatus.SUCCESS);
    return ResponseEntity.status(HttpStatus.OK).body(response);
  }

  /**
   * URL: /api/v1/category
   * Description: Get category details.
   * Method: GET
   *
   * @return category details
   */
  @GetMapping(ID_ENDPOINT)
  @Timed
  public ResponseEntity<ApiResponse<Category>> getCategory(@PathVariable("id") Long categoryId) {
    ApiResponse<Category> response = new ApiResponse<>();

    try {
      Category category = categoryService.getCategoryDetails(categoryId);
      response.setData(category);
      response.setStatus(ApiResponseStatus.SUCCESS);
      return ResponseEntity.status(HttpStatus.OK).body(response);
    } catch (EntityDoesNotExistException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(e.getMessage());
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }
  }

  /**
   * URL: /api/v1/category
   * Description: Add category
   * Method: POST
   *
   * @param category of type Category
   * @return Created category details
   */
  @PostMapping
  @PreAuthorize("hasAuthority('ROLE_ADMIN')")
  @Timed
  public ResponseEntity<ApiResponse<Category>> addCategory(@RequestBody @Valid Category category) {
    // Creating item for the user
    Category addedCategory = categoryService.addCategory(category);

    ApiResponse<Category> response = new ApiResponse<>();
    response.setData(addedCategory);
    response.setStatus(ApiResponseStatus.SUCCESS);
    return ResponseEntity.status(HttpStatus.OK).body(response);
  }

  /**
   * URL: /api/v1/category/{id}
   * Description: Update category details
   * Method: PUT
   *
   * @param category of type Category
   * @return updated category details
   */
  @PutMapping(ID_ENDPOINT)
  @PreAuthorize("hasAuthority('ROLE_ADMIN')")
  @Timed
  public ResponseEntity<ApiResponse<Category>> updateCategory(
      @PathVariable("id") Long categoryId, @RequestBody @Valid Category category) {
    ApiResponse<Category> response = new ApiResponse<>();
    try {
      Category updateCategory = categoryService.updateCategory(categoryId, category);
      response.setData(updateCategory);
      response.setStatus(ApiResponseStatus.SUCCESS);
      return ResponseEntity.status(HttpStatus.OK).body(response);
    } catch (EntityDoesNotExistException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(String.format(e.getMessage()));
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }
  }

  /**
   * URL: /api/v1/category/{id}
   * Description: Delete category of given category id
   * Method: DELETE
   */
  @DeleteMapping(ID_ENDPOINT)
  @PreAuthorize("hasAuthority('ROLE_ADMIN')")
  @Timed
  public ResponseEntity<ApiResponse<String>> deleteOrder(@PathVariable("id") Long categoryId) {
    categoryService.deleteCategory(categoryId);
    ApiResponse<String> response = new ApiResponse<>();
    response.setStatus(ApiResponseStatus.SUCCESS);
    response.setMessage("Category deleted");
    return ResponseEntity.status(HttpStatus.OK).body(response);
  }
}
