package com.crio.zcommerce.backend.product.service.implementation;

import com.crio.zcommerce.backend.product.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.product.dao.ItemDao;
import com.crio.zcommerce.backend.product.dao.ReviewDao;
import com.crio.zcommerce.backend.product.model.Item;
import com.crio.zcommerce.backend.product.model.Review;
import com.crio.zcommerce.backend.product.service.ReviewService;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@Log4j2
@RequiredArgsConstructor
public class ReviewServiceImpl implements ReviewService {

  private final ReviewDao reviewDao;

  private final ItemDao itemDao;

  @Override
  public Review getReviewDetails(Long reviewId) throws EntityDoesNotExistException {
    String logPrefix = "event: getReviewDetails, message: ";
    log.info(logPrefix + "fetching details of reviewId: {}", reviewId);
    Optional<Review> reviewOptional = reviewDao.findByReviewId(reviewId);
    if (reviewOptional.isPresent()) {
      Review review = reviewOptional.get();
      log.info(logPrefix + "review details fetch for reviewId : {}, review : {}", reviewId, review);
      return review;
    } else {
      log.info(logPrefix + "review details not found for the reviewId : {}", reviewId);
      throw new EntityDoesNotExistException(
          String.format("%s review details not found for the reviewId : %s", logPrefix, reviewId));
    }
  }

  @Override
  public Page<Review> getReviewsOfItem(Long itemId, Pageable pageable)
      throws EntityDoesNotExistException {
    String logPrefix = "event: getReviewsOfItem, message: ";
    log.info(logPrefix + "fetching reviews of the itemId : {}", itemId);
    return reviewDao.findAllByItemId(itemId, pageable);
  }

  @Override
  public Review addReview(Review review) throws EntityDoesNotExistException {
    String logPrefix = "event: addReview, message: ";
    log.info(logPrefix + "adding new Review: {}", review);

    Optional<Item> itemOptional = itemDao.findByItemId(review.getItem().getId());
    Item item =
        itemOptional.orElseThrow(
            () ->
                new EntityDoesNotExistException(
                    String.format(
                        "%s Item details not found for the itemId : %s",
                        logPrefix, review.getItem().getId())));
    review.setItem(item);

    Review addedReview = reviewDao.addReview(review);
    log.info(logPrefix + "new review got added, id: {}", addedReview.getId());

    Integer updatedNumberOfReviews = item.getNumberOfReviews() + 1;
    Float updatedAverageRating =
        getUpdatedAvgRating(
            item.getAverageRating(), item.getNumberOfReviews(), review.getRating(), 1);
    item.setAverageRating(updatedAverageRating);
    item.setNumberOfReviews(updatedNumberOfReviews);
    itemDao.updateItem(item.getId(), item);
    addedReview.setItem(item);
    log.info(logPrefix + "updated item average rating, itemId: {}", item.getId());

    return addedReview;
  }

  @Override
  public Review updateReview(Long reviewId, Review review) throws EntityDoesNotExistException {
    String logPrefix = "event: updateReview, message: ";
    log.info(logPrefix + "updating review for reviewId: ", reviewId);

    // Get Item
    Optional<Item> itemOptional = itemDao.findByItemId(review.getItem().getId());
    Item item =
        itemOptional.orElseThrow(
            () ->
                new EntityDoesNotExistException(
                    String.format(
                        "%s Item details not found for the itemId : %s",
                        logPrefix, review.getItem().getId())));
    review.setItem(item);

    // Get Existing Review
    Review existingReview =
        reviewDao
            .findByReviewId(reviewId)
            .orElseThrow(
                () -> {
                  log.info(
                      "{} review details not found for the reviewId : {}", logPrefix, reviewId);
                  return new EntityDoesNotExistException(
                      String.format(
                          "%s update failed as review details not found for the reviewId : %s",
                          logPrefix, reviewId));
                });

    // Update review
    Review updatedReview = reviewDao.updateReview(reviewId, review);
    log.info(
        logPrefix + "Review has been updated, id: {} , Review: {}",
        updatedReview.getId(),
        updatedReview);

    // Update Item's averageRating
    Float updatedAverageRating =
        getUpdatedAvgRating(
            item.getAverageRating(),
            item.getNumberOfReviews(),
            review.getRating() - existingReview.getRating(),
            0);
    item.setAverageRating(updatedAverageRating);
    itemDao.updateItem(item.getId(), item);
    updatedReview.setItem(item);
    log.info(logPrefix + "updated item average rating, itemId: {}", item.getId());
    return updatedReview;
  }

  @Override
  public void deleteReview(Long reviewId) throws EntityDoesNotExistException {
    String logPrefix = "event: deleteReview, message: ";
    log.info(logPrefix + "deleting review having id: ", reviewId);

    // Get Existing Review
    Review existingReview =
        reviewDao
            .findByReviewId(reviewId)
            .orElseThrow(
                () -> {
                  log.info(
                      "{} review details not found for the reviewId : {}", logPrefix, reviewId);
                  return new EntityDoesNotExistException(
                      String.format(
                          "%s update failed as review details not found for the reviewId : %s",
                          logPrefix, reviewId));
                });

    Optional<Item> itemOptional = itemDao.findByItemId(existingReview.getItem().getId());
    Item item =
        itemOptional.orElseThrow(
            () ->
                new EntityDoesNotExistException(
                    String.format(
                        "%s Item details not found for the itemId : %s",
                        logPrefix, existingReview.getItem().getId())));

    log.info(logPrefix + "Review is being deleted, id: {}", reviewId);
    reviewDao.deleteByReviewId(reviewId);

    // Update Item's averageRating
    Integer updatedNumberOfReviews = item.getNumberOfReviews() - 1;
    Float updatedAverageRating =
        getUpdatedAvgRating(
            item.getAverageRating(), item.getNumberOfReviews(), -existingReview.getRating(), -1);
    item.setAverageRating(updatedAverageRating);
    item.setNumberOfReviews(updatedNumberOfReviews);
    itemDao.updateItem(item.getId(), item);
    log.info(logPrefix + "updated item average rating, itemId: {}", item.getId());
  }

  private Float getUpdatedAvgRating(
      Float currentAvg, Integer currentCount, Integer delta, Integer countDelta) {

    return currentCount + countDelta == 0
        ? 0
        : (currentAvg * currentCount + delta) / (currentCount + countDelta);
  }
}
