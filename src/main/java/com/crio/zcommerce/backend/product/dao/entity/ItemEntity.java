package com.crio.zcommerce.backend.product.dao.entity;

import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;
import org.hibernate.Hibernate;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(
    name = "item",
    uniqueConstraints = {@UniqueConstraint(columnNames = "code")},
    indexes = {
      @Index(name = "IDX_code", columnList = "code"),
      @Index(name = "IDX_name", columnList = "name"),
      @Index(name = "IDX_misc", columnList = "name, title, shortDescription")
    })
public class ItemEntity extends BaseEntity {

  @Column(nullable = false)
  private String code;

  @Column(nullable = false)
  private String name;

  @Column(nullable = false)
  private String title;

  @Column(nullable = false)
  @ElementCollection
  @ToString.Exclude
  private List<String> imageUrls;

  @Column(nullable = false)
  private Double price;

  @Column(nullable = false)
  private Float discountPercentage = 0f;

  @Column(nullable = false)
  private String shortDescription;

  @Lob private String longDescription;

  @OneToOne
  @JoinColumn(name = "inventory_entity_id")
  @Exclude
  private InventoryEntity inventory;

  @OneToMany(mappedBy = "item")
  @Exclude
  private Set<ReviewEntity> reviewsList;

  private Float averageRating = 0f;

  private Integer numberOfReviews = 0;

  @ManyToOne
  @JoinColumn(name = "category_id")
  @Exclude
  private CategoryEntity category;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
      return false;
    }
    ItemEntity that = (ItemEntity) o;
    return getId() != null && Objects.equals(getId(), that.getId());
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}
