package com.crio.zcommerce.backend.product.controller.requests;

import com.crio.zcommerce.backend.product.model.Inventory;
import com.crio.zcommerce.backend.product.model.Review;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateItemRequest {
  @NotNull private String code;

  @NotNull private String name;

  @NotNull private String title;

  @NotNull private List<String> imageUrls;

  @NotNull private Double price;

  @NotNull private String shortDescription;

  private String longDescription;

  private Inventory inventory;

  private Set<Review> reviewsList;

  @NotNull private Long categoryId;
}
