package com.crio.zcommerce.backend.product.dao;

import com.crio.zcommerce.backend.product.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.product.model.Category;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CategoryDao {

  Page<Category> findAllCategory(Pageable pageable);

  Optional<Category> findByCategoryId(Long categoryId);

  Category addCategory(Category category);

  Category updateCategory(Long categoryId, Category category) throws EntityDoesNotExistException;

  void deleteCategory(Long categoryId);
}
