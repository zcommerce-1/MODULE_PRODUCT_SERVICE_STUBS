package com.crio.zcommerce.backend.product.service.implementation;

import com.crio.zcommerce.backend.product.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.product.dao.CategoryDao;
import com.crio.zcommerce.backend.product.model.Category;
import com.crio.zcommerce.backend.product.service.CategoryService;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@Log4j2
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

  private final CategoryDao categoryDao;

  @Override
  public Page<Category> getAllCategory(Pageable pageable) {
    String logPrefix = "event: getAllCategory, message: ";
    log.info(logPrefix + "fetching all categories");
    return categoryDao.findAllCategory(pageable);
  }

  @Override
  public Category getCategoryDetails(Long categoryId) throws EntityDoesNotExistException {
    String logPrefix = "event: getCategoryDetails, message: ";
    Optional<Category> categoryOptional = categoryDao.findByCategoryId(categoryId);
    if (categoryOptional.isPresent()) {
      Category category = categoryOptional.get();
      log.info(
          logPrefix + "category details fetch for categoryId : {}, category : {}",
          categoryId,
          category);
      return category;
    } else {
      log.info(logPrefix + "category details not found for the categoryId : {}", categoryId);
      throw new EntityDoesNotExistException(
          String.format(
              "%s category details not found for the categoryId : %s", logPrefix, categoryId));
    }
  }

  @Override
  public Category addCategory(Category category) {
    String logPrefix = "event: addCategory, message: ";
    Category addedCategory = categoryDao.addCategory(category);
    log.info(
        logPrefix + "new category got added, id: {} , code: {}",
        addedCategory.getId(),
        addedCategory.getCode());
    return addedCategory;
  }

  @Override
  public Category updateCategory(Long categoryId, Category category)
      throws EntityDoesNotExistException {
    String logPrefix = "event: updateCategory, message: ";
    Category updatedCategory = categoryDao.updateCategory(categoryId, category);
    log.info(
        logPrefix + "Category has been updated, id: {} , Category: {}",
        updatedCategory.getId(),
        updatedCategory);
    return updatedCategory;
  }

  @Override
  public void deleteCategory(Long categoryId) {
    String logPrefix = "event: deleteCategory, message: ";
    log.info(logPrefix + "Category is being deleted, id: {}", categoryId);
    categoryDao.deleteCategory(categoryId);
  }
}
