package com.crio.zcommerce.backend.product.dao.repository;

import com.crio.zcommerce.backend.product.dao.entity.InventoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InventoryRepository extends JpaRepository<InventoryEntity, Long> {}
