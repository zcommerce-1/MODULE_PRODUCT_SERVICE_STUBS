package com.crio.zcommerce.backend.product.service;

import com.crio.zcommerce.backend.product.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.product.model.Review;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ReviewService {

  /**
   * Get review details for the given review Id
   *
   * @param reviewId of type Long
   * @return review details if present
   * @throws EntityDoesNotExistException when review details is not found against given reviewId
   */
  Review getReviewDetails(Long reviewId) throws EntityDoesNotExistException;

  /**
   * Get review details for the given item Id
   *
   * @param itemId of type Long
   * @return review details if present
   * @throws EntityDoesNotExistException when review details is not found against given item
   */
  Page<Review> getReviewsOfItem(Long itemId, Pageable pageable) throws EntityDoesNotExistException;

  /**
   * Add or create review
   *
   * @param review of type Review
   * @return review details of the actual review created
   */
  Review addReview(Review review) throws EntityDoesNotExistException;

  /**
   * Update review
   *
   * @param reviewId of type long
   * @param review of type Review to update
   * @return updated item details
   * @throws EntityDoesNotExistException when review details is not found against given reviewId
   */
  Review updateReview(Long reviewId, Review review) throws EntityDoesNotExistException;

  /**
   * Delete review details
   *
   * @param reviewId of type Long
   */
  void deleteReview(Long reviewId) throws EntityDoesNotExistException;
}
