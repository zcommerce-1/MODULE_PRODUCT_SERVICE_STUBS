package com.crio.zcommerce.backend.product.dao.repository;

import com.crio.zcommerce.backend.product.dao.entity.CategoryEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<CategoryEntity, Long> {

  Page<CategoryEntity> findAll(Pageable pageable);
}
