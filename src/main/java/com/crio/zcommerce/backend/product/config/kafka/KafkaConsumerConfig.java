package com.crio.zcommerce.backend.product.config.kafka;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;

@EnableKafka
@Configuration
@Data
public class KafkaConsumerConfig {

  @Autowired private KafkaConfig kafkaConfig;

  /*
   this class contains configuration of the consumers
  */
}
