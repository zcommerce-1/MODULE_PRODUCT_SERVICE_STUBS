package com.crio.zcommerce.backend.product.dao.implementation;

import com.crio.zcommerce.backend.product.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.product.dao.CategoryDao;
import com.crio.zcommerce.backend.product.dao.entity.CategoryEntity;
import com.crio.zcommerce.backend.product.dao.repository.CategoryRepository;
import com.crio.zcommerce.backend.product.model.Category;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Log4j2
@RequiredArgsConstructor
public class CategoryDaoImpl implements CategoryDao {

  private final CategoryRepository categoryRepository;

  private final ModelMapper modelMapper;

  @Override
  @Transactional
  public Page<Category> findAllCategory(Pageable pageable) {
    Page<CategoryEntity> categoryEntityPaged = categoryRepository.findAll(pageable);
    return categoryEntityPaged.map(
        categoryEntity -> modelMapper.map(categoryEntity, Category.class));
  }

  @Override
  @Transactional
  public Optional<Category> findByCategoryId(Long categoryId) {
    Optional<CategoryEntity> categoryEntityOptional = categoryRepository.findById(categoryId);

    // check if category details is present
    if (categoryEntityOptional.isPresent()) {
      Category category = modelMapper.map(categoryEntityOptional.get(), Category.class);
      return Optional.of(category);
    }

    // returning empty optional as cart details is not present
    return Optional.empty();
  }

  @Override
  @Transactional
  public Category addCategory(Category category) {
    CategoryEntity categoryEntity = modelMapper.map(category, CategoryEntity.class);

    // inserting cart entity into cart table
    CategoryEntity addedCategory = categoryRepository.save(categoryEntity);
    return modelMapper.map(addedCategory, Category.class);
  }

  @Override
  @Transactional
  public Category updateCategory(Long categoryId, Category category)
      throws EntityDoesNotExistException {
    String logPrefix = "event: updateCategory, message: ";

    // fetching the existing cart entity for the cartId
    Optional<CategoryEntity> existingCategoryEntityOptional =
        categoryRepository.findById(categoryId);
    if (!existingCategoryEntityOptional.isPresent()) {
      log.info(logPrefix + "category details not found for the categoryId : {}", categoryId);
      throw new EntityDoesNotExistException(
          String.format(
              "%s update failed as category details not found for the categoryId  : %s",
              logPrefix, categoryId));
    }

    CategoryEntity categoryEntity = existingCategoryEntityOptional.get();
    // todo use mapper and a patch request object
    categoryEntity.setCode(category.getCode());
    categoryEntity.setName(category.getName());
    return modelMapper.map(categoryRepository.save(categoryEntity), Category.class);
  }

  @Override
  public void deleteCategory(Long categoryId) {
    categoryRepository.deleteById(categoryId);
  }
}
