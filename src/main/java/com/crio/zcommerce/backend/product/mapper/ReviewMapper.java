package com.crio.zcommerce.backend.product.mapper;

import com.crio.zcommerce.backend.product.dao.entity.ReviewEntity;
import com.crio.zcommerce.backend.product.model.Review;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

@Mapper(
    unmappedTargetPolicy = ReportingPolicy.IGNORE,
    componentModel = "spring",
    uses = ItemMapper.class)
public interface ReviewMapper {

  Review toDto(ReviewEntity reviewEntity);

  ReviewEntity toEntity(Review review);

  @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
  ReviewEntity updateEntity(Review review, @MappingTarget ReviewEntity reviewEntity);
}
