package com.crio.zcommerce.backend.product.dao.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import java.util.Objects;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;
import org.hibernate.Hibernate;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(
    name = "review",
    uniqueConstraints = {@UniqueConstraint(columnNames = "id")},
    indexes = @Index(name = "IDX_reviewerName", columnList = "reviewerName"))
public class ReviewEntity extends BaseEntity {

  @ManyToOne
  @JoinColumn(name = "item_id")
  @Exclude
  private ItemEntity item;

  @Column(nullable = false)
  // todo this should be of type user
  private String reviewerName;

  @Column(nullable = false)
  private String message;

  @Column(nullable = false)
  @Min(value = 1, message = "The rating must be from 1 to 5.")
  @Max(value = 5, message = "The rating must be from 1 to 5.")
  private Integer rating;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
      return false;
    }
    ReviewEntity that = (ReviewEntity) o;
    return getId() != null && Objects.equals(getId(), that.getId());
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}
