package com.crio.zcommerce.backend.product.mapper;

import com.crio.zcommerce.backend.product.dao.entity.ItemEntity;
import com.crio.zcommerce.backend.product.model.Item;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface ItemMapper {

  @Mapping(target = "reviewsList", ignore = true)
  Item toDto(ItemEntity itemEntity);

  ItemEntity toEntity(Item item);

  @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
  ItemEntity updateEntity(Item item, @MappingTarget ItemEntity itemEntity);
}
