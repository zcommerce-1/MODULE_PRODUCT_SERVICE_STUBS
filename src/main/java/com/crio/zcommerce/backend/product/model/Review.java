package com.crio.zcommerce.backend.product.model;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class Review extends BaseModel {

  private Item item;

  @NotNull(message = "reviewerName should not be null")
  // todo this should be of type user
  private String reviewerName;

  @NotNull(message = "message should not be null.") private String message;

  @NotNull(message = "Rating should not be null.") @Min(value = 1, message = "The rating must be from 1 to 5.")
  @Max(value = 5, message = "The rating must be from 1 to 5.")
  private Integer rating;
}
