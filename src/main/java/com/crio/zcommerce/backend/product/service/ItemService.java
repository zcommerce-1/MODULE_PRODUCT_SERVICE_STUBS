package com.crio.zcommerce.backend.product.service;

import com.crio.zcommerce.backend.product.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.product.model.Inventory;
import com.crio.zcommerce.backend.product.model.Item;
import com.crio.zcommerce.backend.product.model.enums.ReviewIncludeType;
import com.crio.zcommerce.backend.product.model.enums.SortBy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ItemService {

  /**
   * Get item details for the given item Id
   *
   * @param itemId of type Long
   * @return item details if present
   * @throws EntityDoesNotExistException when item details is not found against given itemId
   */
  Item getItemDetails(Long itemId, ReviewIncludeType reviewIncludeType)
      throws EntityDoesNotExistException;

  /**
   * Add or create item
   *
   * @param item of type Item
   * @return item details of the actual item created
   */
  Item addItem(Item item) throws EntityDoesNotExistException;

  /**
   * Update item
   *
   * @param itemId of type long
   * @param item of type Cart to update
   * @return updated item details
   */
  Item updateItem(Long itemId, Item item) throws EntityDoesNotExistException;

  /**
   * Delete item details
   *
   * @param itemId of type Long
   */
  void deleteItem(Long itemId);

  /**
   * Search for items based on the keyword. Keyword can be part of items name, title, short and
   * long description
   *
   * @param searchKeyword of type String
   * @param sortBy of type SortBy
   * @param pageable The pagination info.
   * @return list of items
   */
  Page<Item> searchForItem(String searchKeyword, SortBy sortBy, Pageable pageable);

  /**
   * Get inventory details of the given itemId
   *
   * @param itemId of type Long
   *
   * @return inventory details of type Inventory
   * @throws EntityDoesNotExistException if inventory details not found for the item
   */
  Inventory getInventoryDetails(Long itemId) throws EntityDoesNotExistException;

  void updateInventory(Long itemId, Inventory inventory);
}
