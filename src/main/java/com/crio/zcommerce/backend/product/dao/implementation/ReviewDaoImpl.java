package com.crio.zcommerce.backend.product.dao.implementation;

import com.crio.zcommerce.backend.product.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.product.dao.ReviewDao;
import com.crio.zcommerce.backend.product.dao.entity.ReviewEntity;
import com.crio.zcommerce.backend.product.dao.repository.ReviewRepository;
import com.crio.zcommerce.backend.product.mapper.ReviewMapper;
import com.crio.zcommerce.backend.product.model.Review;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Log4j2
@RequiredArgsConstructor
public class ReviewDaoImpl implements ReviewDao {

  private final ReviewRepository reviewRepository;

  private final ReviewMapper reviewMapper;

  @Override
  @Transactional
  public Optional<Review> findByReviewId(Long reviewId) {
    Optional<ReviewEntity> reviewEntityOptional = reviewRepository.findById(reviewId);

    // check if review details is present
    if (reviewEntityOptional.isPresent()) {
      Review review = reviewMapper.toDto(reviewEntityOptional.get());
      return Optional.of(review);
    }

    // returning empty optional as cart details is not present
    return Optional.empty();
  }

  @Override
  @Transactional
  public Page<Review> findAllByItemId(Long itemId, Pageable pageable) {
    Page<ReviewEntity> reviewEntityList = reviewRepository.findByItemId(itemId, pageable);
    return reviewEntityList.map(
        reviewEntity -> {
          Review review = reviewMapper.toDto(reviewEntity);
          review.setItem(null);
          return review;
        });
  }

  @Override
  @Transactional
  public Review addReview(Review review) {
    ReviewEntity reviewEntity = reviewMapper.toEntity(review);

    // inserting review entity into cart table
    ReviewEntity reviewEntity1 = reviewRepository.save(reviewEntity);
    return reviewMapper.toDto(reviewEntity1);
  }

  @Override
  @Transactional
  public Review updateReview(Long reviewId, Review review) throws EntityDoesNotExistException {
    String logPrefix = "event: updateReview, message: ";

    // fetching the existing cart entity for the cartId
    Optional<ReviewEntity> existingReviewEntityOptional = reviewRepository.findById(reviewId);

    if (!existingReviewEntityOptional.isPresent()) {
      log.info(logPrefix + "review details not found for the reviewId : {}", reviewId);
      throw new EntityDoesNotExistException(
          String.format(
              "%s update failed as review details not found for the reviewId : %s",
              logPrefix, reviewId));
    }

    ReviewEntity reviewEntity = existingReviewEntityOptional.get();
    reviewMapper.updateEntity(review, reviewEntity);
    // todo use a patch request object
    return reviewMapper.toDto(reviewRepository.save(reviewEntity));
  }

  @Override
  public void deleteByReviewId(Long reviewId) {
    reviewRepository.deleteById(reviewId);
  }
}
