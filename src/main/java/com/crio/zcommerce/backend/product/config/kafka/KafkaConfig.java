package com.crio.zcommerce.backend.product.config.kafka;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;

@EnableKafka
@Configuration
@ConfigurationProperties(prefix = "kafka")
@Data
public class KafkaConfig {

  @Value("${kafka.topic.order.name}")
  private String orderEventTopic;

  private String bootstrapAddress;
}
