package com.crio.zcommerce.backend.product.dao.implementation;

import com.crio.zcommerce.backend.product.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.product.dao.ItemDao;
import com.crio.zcommerce.backend.product.dao.entity.InventoryEntity;
import com.crio.zcommerce.backend.product.dao.entity.ItemEntity;
import com.crio.zcommerce.backend.product.dao.queries.ItemQuery;
import com.crio.zcommerce.backend.product.dao.repository.InventoryRepository;
import com.crio.zcommerce.backend.product.dao.repository.ItemRepository;
import com.crio.zcommerce.backend.product.mapper.ItemMapper;
import com.crio.zcommerce.backend.product.model.Inventory;
import com.crio.zcommerce.backend.product.model.Item;
import com.crio.zcommerce.backend.product.model.enums.SortBy;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Log4j2
@RequiredArgsConstructor
public class ItemDaoImpl implements ItemDao {

  private final ItemRepository itemRepository;

  private final ModelMapper modelMapper;

  private final ItemMapper itemMapper;

  private final InventoryRepository inventoryRepository;

  @Override
  @Transactional
  public Optional<Item> findByItemId(Long itemId) {
    Optional<ItemEntity> itemEntityOptional = itemRepository.findById(itemId);

    // check if cart details is present
    if (itemEntityOptional.isPresent()) {
      Item item = itemMapper.toDto(itemEntityOptional.get());
      return Optional.of(item);
    }

    // returning empty optional as cart details is not present
    return Optional.empty();
  }

  @Override
  @Transactional
  public Item addItem(Item item) {
    InventoryEntity inventoryEntity = modelMapper.map(item.getInventory(), InventoryEntity.class);
    inventoryRepository.save(inventoryEntity);

    ItemEntity itemEntity = itemMapper.toEntity(item);
    itemEntity.setInventory(inventoryEntity);

    return itemMapper.toDto(itemRepository.save(itemEntity));
  }

  @Override
  @Transactional
  public Item updateItem(Long itemId, Item item) throws EntityDoesNotExistException {
    String logPrefix = "event: updateItem, message: ";

    ItemEntity existingItemEntity =
        itemRepository
            .findById(itemId)
            .orElseThrow(
                () ->
                    new EntityDoesNotExistException(
                        String.format(
                            "%s update failed as item details not found for the itemId : %s",
                            logPrefix, itemId)));

    ItemEntity itemEntity = itemMapper.updateEntity(item, existingItemEntity);
    // todo use a patch request object
    return itemMapper.toDto(itemRepository.save(itemEntity));
  }

  @Override
  public void deleteItem(Long itemId) {
    itemRepository.deleteById(itemId);
  }

  @Override
  @Transactional
  public Page<Item> searchForItem(String searchKeyword, SortBy sortBy, Pageable pageable) {
    Sort sort =
        sortBy != null
            ? switch (sortBy) {
              case DATE_ADDED -> Sort.by("createdAt").descending();
              case PRICE_HIGH_TO_LOW -> Sort.by("price").descending();
              case PRICE_LOW_TO_HIGH -> Sort.by("price").ascending();
              default -> pageable
                  .getSort(); // use the original sort if sortBy doesn't match any of the known
                // values
            }
            : pageable.getSort(); // use the original sort if sortBy is null
    Pageable sortedPageable =
        PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);

    Page<ItemEntity> itemEntities =
        itemRepository.findAll(ItemQuery.containsKeyword(searchKeyword), sortedPageable);

    return itemEntities.map(itemMapper::toDto);
  }

  @Override
  public Inventory addInventory(Inventory inventory) {
    InventoryEntity inventoryEntity = modelMapper.map(inventory, InventoryEntity.class);
    InventoryEntity createdInventoryEntity = inventoryRepository.save(inventoryEntity);
    return modelMapper.map(createdInventoryEntity, Inventory.class);
  }

  @Override
  public Optional<Inventory> findInventoryByItemId(Long itemId) {
    Optional<ItemEntity> itemEntityOptional = itemRepository.findById(itemId);

    // check if item details is present
    if (itemEntityOptional.isPresent()) {
      InventoryEntity inventoryEntity = itemEntityOptional.get().getInventory();
      Inventory inventory = modelMapper.map(inventoryEntity, Inventory.class);
      return Optional.of(inventory);
    }

    return Optional.empty();
  }

  @Override
  public void updateInventory(Long itemId, Inventory inventory) {
    Optional<ItemEntity> itemEntityOptional = itemRepository.findById(itemId);

    // check if item details is present
    if (itemEntityOptional.isPresent()) {
      ItemEntity itemEntity = itemEntityOptional.get();
      InventoryEntity inventoryEntity = itemEntity.getInventory();
      inventoryEntity.setQuantityInStock(inventory.getQuantityInStock());
      itemEntity.setInventory(inventoryEntity);
      itemRepository.save(itemEntity);
    }
  }
}
