package com.crio.zcommerce.backend.product.controller;

import com.crio.zcommerce.backend.product.common.api.ApiResponse;
import com.crio.zcommerce.backend.product.common.api.ApiResponseStatus;
import com.crio.zcommerce.backend.product.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.product.model.Inventory;
import com.crio.zcommerce.backend.product.model.Item;
import com.crio.zcommerce.backend.product.model.enums.ReviewIncludeType;
import com.crio.zcommerce.backend.product.model.enums.SortBy;
import com.crio.zcommerce.backend.product.service.ItemService;
import jakarta.validation.Valid;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.metrics.annotation.Timed;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Timed
@RestController
@RequiredArgsConstructor
@RequestMapping(ItemController.ITEM_API_PREFIX)
public class ItemController {

  public static final String ITEM_API_PREFIX = "/api/v1/item";
  private static final String ID_ENDPOINT = "/{id:\\d+}";

  private static final String ITEM_SEARCH_ENDPOINT = "/search";

  private static final String INVENTORY_ENDPOINT = "/inventory";

  private static final String RECOMMENDED_ENDPOINT = "/recommended";

  private final ItemService itemService;

  /**
   * URL: /api/v1/item/{id}
   * Description: Get item details of the item.
   * Method: GET
   *
   * @return item details
   */
  @GetMapping(ID_ENDPOINT)
  @Timed
  public ResponseEntity<ApiResponse<Item>> getItemDetails(
      @PathVariable("id") Long itemId,
      @RequestParam(value = "includeReview", required = false)
          ReviewIncludeType reviewIncludeType) {
    ApiResponse<Item> response = new ApiResponse<>();

    if (Objects.isNull(reviewIncludeType)) {
      reviewIncludeType = ReviewIncludeType.NOT_INCLUDE;
    }
    try {
      Item item = itemService.getItemDetails(itemId, reviewIncludeType);
      response.setData(item);
      response.setStatus(ApiResponseStatus.SUCCESS);
      return ResponseEntity.status(HttpStatus.OK).body(response);
    } catch (EntityDoesNotExistException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(e.getMessage());
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }
  }

  /**
   * URL: /api/v1/item/{searchKeyword}
   * Description: Get list of items of specific keyword pattern.
   * Method: GET
   *
   * @return list of item details
   */
  @GetMapping(ITEM_SEARCH_ENDPOINT)
  @Timed
  public ResponseEntity<ApiResponse<Page<Item>>> searchItemDetails(
      @RequestParam(value = "keyword", required = false) String keyword,
      @RequestParam(value = "sortBy", required = false) SortBy sortBy,
      @RequestParam(defaultValue = "0") Integer page,
      @RequestParam(defaultValue = "20") Integer size) {
    ApiResponse<Page<Item>> response = new ApiResponse<>();
    Page<Item> item = itemService.searchForItem(keyword, sortBy, PageRequest.of(page, size));
    response.setData(item);
    response.setStatus(ApiResponseStatus.SUCCESS);
    return ResponseEntity.status(HttpStatus.OK).body(response);
  }

  /**
   * URL: /api/v1/item
   * Description: Add item
   * Method: POST
   *
   * @param item of type Item
   * @return Created item details
   */
  @PostMapping
  @PreAuthorize("hasAuthority('ROLE_ADMIN')")
  @Timed
  public ResponseEntity<ApiResponse<Item>> addItem(@RequestBody @Valid Item item)
      throws EntityDoesNotExistException {
    // Creating item for the user
    Item createdItem = itemService.addItem(item);

    ApiResponse<Item> response = new ApiResponse<>();
    response.setData(createdItem);
    response.setStatus(ApiResponseStatus.SUCCESS);
    return ResponseEntity.status(HttpStatus.OK).body(response);
  }

  /**
   * URL: /api/v1/item/{id}
   * Description: Update item details
   * Method: PUT
   *
   * @param item of type Cart
   * @return updated item details
   */
  @PutMapping(ID_ENDPOINT)
  @PreAuthorize("hasAuthority('ROLE_ADMIN')")
  @Timed
  public ResponseEntity<ApiResponse<Item>> updateItem(
      @PathVariable("id") Long itemId, @RequestBody @Valid Item item) {
    ApiResponse<Item> response = new ApiResponse<>();
    try {
      Item updatedItem = itemService.updateItem(itemId, item);
      response.setData(updatedItem);
      response.setStatus(ApiResponseStatus.SUCCESS);
      return ResponseEntity.status(HttpStatus.OK).body(response);
    } catch (EntityDoesNotExistException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(String.format(e.getMessage()));
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }
  }

  /**
   * URL: /api/v1/item/{id}
   * Description: Delete item of given item id
   * Method: DELETE
   */
  @DeleteMapping(ID_ENDPOINT)
  @PreAuthorize("hasAuthority('ROLE_ADMIN')")
  @Timed
  public ResponseEntity<ApiResponse<String>> deleteItem(@PathVariable("id") Long itemId) {
    itemService.deleteItem(itemId);
    ApiResponse<String> response = new ApiResponse<>();
    response.setStatus(ApiResponseStatus.SUCCESS);
    response.setMessage("Item deleted");
    return ResponseEntity.status(HttpStatus.OK).body(response);
  }

  /**
   * URL: /api/v1/item/inventory
   * Description: Get inventory details of the item.
   * Method: GET
   *
   * @return inventory details
   */
  @GetMapping(ID_ENDPOINT + INVENTORY_ENDPOINT)
  @Timed
  public ResponseEntity<ApiResponse<Inventory>> getInventoryDetails(
      @PathVariable("id") Long itemId) {
    ApiResponse<Inventory> response = new ApiResponse<>();

    try {
      Inventory inventory = itemService.getInventoryDetails(itemId);
      response.setData(inventory);
      response.setStatus(ApiResponseStatus.SUCCESS);
      return ResponseEntity.status(HttpStatus.OK).body(response);
    } catch (EntityDoesNotExistException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(e.getMessage());
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }
  }

  /**
   * URL: /api/v1/item/inventory
   * Description: Update inventory details
   * Method: PUT
   *
   * @param inventory of type Inventory
   */
  @PutMapping(ID_ENDPOINT + INVENTORY_ENDPOINT)
  @PreAuthorize("hasAuthority('ROLE_ADMIN')")
  @Timed
  public ResponseEntity<ApiResponse<Item>> updateInventoryDetails(
      @PathVariable("id") Long inventoryId, @RequestBody @Valid Inventory inventory) {
    ApiResponse<Item> response = new ApiResponse<>();
    itemService.updateInventory(inventoryId, inventory);
    response.setStatus(ApiResponseStatus.SUCCESS);
    return ResponseEntity.status(HttpStatus.OK).body(response);
  }

  /**
   * URL: /api/v1/item/recommended
   * Description: Get recommended list of the item.
   * Method: GET
   *
   * @return list of recommended items
   */
  @GetMapping(RECOMMENDED_ENDPOINT)
  @Timed
  public ResponseEntity<ApiResponse<Page<Item>>> getRecommendedItems() {

    ApiResponse<Page<Item>> response = new ApiResponse<>();
    Page<Item> item =
        itemService.searchForItem(null, SortBy.PRICE_HIGH_TO_LOW, PageRequest.of(0, 20));
    response.setData(item);
    response.setStatus(ApiResponseStatus.SUCCESS);
    return ResponseEntity.status(HttpStatus.OK).body(response);
  }
}
