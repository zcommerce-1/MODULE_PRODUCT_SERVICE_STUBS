package com.crio.zcommerce.backend.product.controller;

import com.crio.zcommerce.backend.product.common.api.ApiResponse;
import com.crio.zcommerce.backend.product.common.api.ApiResponseStatus;
import com.crio.zcommerce.backend.product.common.exceptions.EntityDoesNotExistException;
import com.crio.zcommerce.backend.product.model.Review;
import com.crio.zcommerce.backend.product.service.ReviewService;
import jakarta.validation.Valid;
import java.security.Principal;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.metrics.annotation.Timed;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Timed
@RestController
@RequiredArgsConstructor
@RequestMapping(ReviewController.REVIEW_API_PREFIX)
public class ReviewController {

  public static final String REVIEW_API_PREFIX = "api/v1/review";
  private static final String ID_ENDPOINT = "/{id}";

  private final ReviewService reviewService;

  /**
   * URL: /api/v1/review/{id}
   * Description: Get review details for given reviewId.
   * Method: GET
   *
   * @return review details
   */
  @GetMapping(ID_ENDPOINT)
  @Timed
  public ResponseEntity<ApiResponse<Review>> getReviewDetails(@PathVariable("id") Long reviewId) {
    ApiResponse<Review> response = new ApiResponse<>();

    try {
      Review review = reviewService.getReviewDetails(reviewId);
      response.setData(review);
      response.setStatus(ApiResponseStatus.SUCCESS);
      return ResponseEntity.status(HttpStatus.OK).body(response);
    } catch (EntityDoesNotExistException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(e.getMessage());
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }
  }

  /**
   * URL: /api/v1/review
   * Description: Add review
   * Method: POST
   *
   * @param review of type Review
   * @return Created review details
   */
  @PostMapping
  @PreAuthorize("hasAuthority('ROLE_USER')")
  @Timed
  public ResponseEntity<ApiResponse<Review>> addReview(
      @RequestBody @Valid Review review, Principal principal) {
    // Creating item for the user
    ApiResponse<Review> response = new ApiResponse<>();
    review.setReviewerName(principal.getName());
    try {
      Review addedReview = reviewService.addReview(review);

      response.setData(addedReview);
      response.setStatus(ApiResponseStatus.SUCCESS);
      return ResponseEntity.status(HttpStatus.OK).body(response);
    } catch (EntityDoesNotExistException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(e.getMessage());
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }
  }

  /**
   * URL: /api/v1/review/{id}
   * Description: Update review details
   * Method: PUT
   *
   * @param review of type Review
   * @return updated review details
   */
  @PutMapping(ID_ENDPOINT)
  @PreAuthorize("hasAuthority('ROLE_USER')")
  @Timed
  public ResponseEntity<ApiResponse<Review>> updateReview(
      @PathVariable("id") Long reviewId, @RequestBody @Valid Review review) {
    ApiResponse<Review> response = new ApiResponse<>();
    try {
      // todo check if review belong to user
      Review updateReview = reviewService.updateReview(reviewId, review);
      response.setData(updateReview);
      response.setStatus(ApiResponseStatus.SUCCESS);
      return ResponseEntity.status(HttpStatus.OK).body(response);
    } catch (EntityDoesNotExistException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(String.format(e.getMessage()));
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }
  }

  /**
   * URL: /api/v1/review/{id}
   * Description: Delete review of given review id
   * Method: DELETE
   */
  @DeleteMapping(ID_ENDPOINT)
  @PreAuthorize("hasAuthority('ROLE_USER')")
  @Timed
  public ResponseEntity<ApiResponse<String>> deleteReview(@PathVariable("id") Long reviewId) {
    ApiResponse<String> response = new ApiResponse<>();
    try {
      // todo check if review being deleted belongs to user.
      reviewService.deleteReview(reviewId);
      response.setStatus(ApiResponseStatus.SUCCESS);
      response.setMessage("Review deleted");
      return ResponseEntity.status(HttpStatus.OK).body(response);
    } catch (EntityDoesNotExistException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(String.format(e.getMessage()));
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }
  }

  /**
   * URL: /api/v1/review/
   * Description: Get all reviews of given itemId
   * Method: GET
   *
   * @param itemId of type Long
   * @return Page of review details
   */
  @GetMapping()
  @Timed
  public ResponseEntity<ApiResponse<Page<Review>>> getReviewsOfItem(
      @RequestParam("itemId") Long itemId,
      @SortDefault(sort = "id") @PageableDefault(size = 20) Pageable pageable) {
    ApiResponse<Page<Review>> response = new ApiResponse<>();

    try {
      Page<Review> review = reviewService.getReviewsOfItem(itemId, pageable);
      response.setData(review);
      response.setStatus(ApiResponseStatus.SUCCESS);
      return ResponseEntity.status(HttpStatus.OK).body(response);
    } catch (EntityDoesNotExistException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(e.getMessage());
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }
  }
}
