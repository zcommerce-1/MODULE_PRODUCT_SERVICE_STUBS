package com.crio.zcommerce.backend.product.dao.queries;

import com.crio.zcommerce.backend.product.dao.entity.ItemEntity;
import jakarta.validation.constraints.NotNull;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.Nullable;

public class ItemQuery {

  public static Specification<ItemEntity> fieldContains(
      @NotNull String fieldName, @Nullable String keyword) {
    return (root, query, cb) -> {
      if (keyword == null || keyword.isEmpty()) {
        return cb.conjunction(); // always returns true
      }

      return cb.like(cb.lower(root.get(fieldName)), "%" + keyword.toLowerCase() + "%");
    };
  }

  public static Specification<ItemEntity> containsKeyword(@Nullable String keyword) {
    if (keyword == null || keyword.isEmpty()) {
      return (root, query, cb) -> cb.conjunction(); // always returns true
    }

    return Specification.where(fieldContains("name", keyword))
        .or(fieldContains("title", keyword))
        .or(fieldContains("shortDescription", keyword));
  }
}
